#!/bin/sh

set -e

ignoreOutdated=0

usage() {
  echo "Usage:"
  echo "$(basename "$0") [options] - print the current latest versions"
  echo "$(basename "$0") [options] latest [stable] - get the latest version"
  echo "$(basename "$0") [options] next [stable|major|minor|patch|pre] - get the next version based on the step"
  echo "$(basename "$0") [options] pre [stable|major|minor|patch|pre [prefix]] - create a prerelease version based on the step"
  echo "$(basename "$0") [options] help - print the usage"
  echo "Options:"
  echo "  -i|--ignore-outdated - don't fetch, don't check for uncommitted changes, don't check for outdated branches"
}

checkGit() {
  if [ "$ignoreOutdated" -eq 1 ]; then
    return
  fi

  currentBranch="$(git branch --show-current)"

  echo "Using branch: $currentBranch"

  if [ -n "$(git status -uno --porcelain)" ]; then
    echo "ERROR: there are uncommitted changes:"
    git status
    exit 1
  fi

  echo "Fetching changes..."
  git fetch --all

  remotes="$(git remote)"

  for remote in $remotes; do
    if ! git merge-base --is-ancestor "$remote"/"$currentBranch" "$currentBranch"; then
      echo "ERROR: The current branch $currentBranch is not up-to-date with the remote $remote/$currentBranch branch:"
      git status
      exit 1
    fi
  done
}

# Get git tags that resemble a SemVer 2.0 version vx.y.z[-prerelease][+build]
getVersionTags() {
  set -e

  # List all tags that look like a version `vx.y.z`
  git tag --list 'v*.*.*' |
  # Filter tags that look like SemVer 2.0 vx.y.z[-prerelease][+build]
  grep -E '^v[0-9]+\.[0-9]+.[0-9]+(-[^\+]+)?(\+.*)?$' |
  # Add underscore to all versions without a hyphen `-`, so v0.0.0_ is sorted as higher version than v0.0.0-alpha
  sed '/-/!s/$/_/' |
  # Sort versions from highest to lowest
  sort -V -r |
  # Remove underscore from the end of all versions
  sed 's/_$//'
}

# Separates a SemVer 2.0 formatted string [v]x.y.z[-prerelease][+build] into 5 lines for each part:
# x, y, z, -prerelease and +build
parseSemVer2() {
  set -e

  version="$1"

  printf "$version" | sed -E 's/^v?([0-9]+)\.([0-9]+)\.([0-9]+)(-[^\+]+)?(\+.*)?$/\1\n\2\n\3\n\4\n\5/'
}

# Gets the next SemVer 2.0 version in the form [v]major.minor.patch[-prerelease][+build].
# Current version defaults to `v0.0.0`.
# This will always strip the `+build` part.
# Possible steps:
# - no step: if the current version has a `-prerelease`, behaves as `pre` step, otherwise, behaves as `patch`.
# - `stable`: if the current version has a `-prerelease`, strips out the prerelease and returns the version as-is. Otherwise, behaves as `patch`.
# - `major`: increments the `major` part by 1, sets `minor` and `patch` to 0 and strips `-prerelease`.
# - `minor`: increments the `minor` part by 1, sets `patch` to 0 and strips `-prerelease`.
# - `patch`: increments the `patch` part by 1 and strips `-prerelease`.
# - `pre`: if the current `-prerelease` is formatted as `-[prefix].<number>`, increments the number by 1. Otherwise, sets `-[currentPrerelease]0` as the prerelease part.
getNextVersion() {
  set -e

  currentVersion="$1"
  step="$2"

  if [ -z "$currentVersion" ]; then
    currentVersion="v0.0.0"
  fi

  currentVersionParsed="$(parseSemVer2 "$currentVersion")"
  major="$(printf "$currentVersionParsed" | sed '1q;d')"
  minor="$(printf "$currentVersionParsed" | sed '2q;d')"
  patch="$(printf "$currentVersionParsed" | sed '3q;d')"
  prerelease="$(printf "$currentVersionParsed" | sed '4q;d')"

  if [ -z "$step" ]; then
    if [ -z "$prerelease" ]; then
      step="patch"
    else
      step="pre"
    fi
  fi

  if [ -n "$prerelease" ] && ([ "$step" = "stable" ] || [ "$step" = "major" ] || [ "$step" = "minor" ] || [ "$step" = "patch" ]); then
    printf "v$major.$minor.$patch"
    return
  fi

  if [ "$step" = "stable" ]; then
    step="patch"
  fi

  case $step in
    major)
      major=$((major + 1))
      minor=0
      patch=0
      prerelease=""
      ;;
    minor)
      minor=$((minor + 1))
      patch=0
      prerelease=""
      ;;
    patch)
      patch=$((patch + 1))
      prerelease=""
      ;;
    pre)
      prereleasePrefix="$(echo "$prerelease" | sed -E 's/^-((.*?\.)[0-9]*|(.*))$/\2\3/')"
      currentPrereleaseVersion="$(echo "$prerelease" | sed -E 's/^-(.*?\.([0-9]+)|.*)$/\2/')"

      if [ -z "$currentPrereleaseVersion" ]; then
        if (echo "$prereleasePrefix" | grep -Eq '[^\.]$'); then
          prereleasePrefix="$prereleasePrefix."
        fi

        prerelease="-${prereleasePrefix}0"
      else
        prerelease="-${prereleasePrefix}$((currentPrereleaseVersion + 1))"
      fi
      ;;
  esac

  printf "v$major.$minor.$patch$prerelease"
}

# Gets the next SemVer 2.0 prerelease version in the form [v]major.minor.patch-prefix.0[+build].
# Current version defaults to `v0.0.0`.
# This will always strip the `+build` part.
# To create the next prerelease version, the next version will be determined by `getNextVersion` using the step, and `-[prefix.]0` will be used as the prerelease part.
createPrerelease() {
  set -e

  currentVersion="$1"
  step="$2"
  prefix="$3"

  if [ -z "$currentVersion" ]; then
    currentVersion="v0.0.0"
  fi

  if [ -z "$step" ]; then
    step="major"
  fi

  if [ -n "$prefix" ] && ! (printf "$prefix" | grep -Eq '\.$'); then
    prefix="$prefix."
  fi

  nextVersion="$(getNextVersion "$currentVersion" "$step")"

  nextVersionParsed="$(parseSemVer2 "$nextVersion")"
  major="$(printf "$nextVersionParsed" | sed '1q;d')"
  minor="$(printf "$nextVersionParsed" | sed '2q;d')"
  patch="$(printf "$nextVersionParsed" | sed '3q;d')"

  printf "v$major.$minor.$patch-${prefix}0"
}

while (echo "$1" | grep -Eq '^-'); do
  case "$1" in
    -i|--ignore-outdated)
      ignoreOutdated=1
      shift
      ;;
    *)
      usage
      exit 1
      ;;
  esac
done

action="$1"

if [ "$action" = "next" ] || [ "$action" = "pre" ] || [ -z "$action" ]; then
  step="$2"
  prefix="$3"

  if [ -n "$action" ]; then
    if [ -n "$step" ] && ! (echo "$step" | grep -Eq '^(stable|major|minor|patch|pre)$'); then
      usage
      exit 1
    fi
  fi

  checkGit

  versions="$(getVersionTags)"
  versionsCount="$(if [ -z "$versions" ]; then echo 0; else echo "$versions" | wc -l; fi)"
  latestVersions="$(echo "$versions" | head -n 5)"
  latestVersionsCount="$(if [ -z "$versions" ]; then echo 0; else echo "$latestVersions" | wc -l; fi)"

  echo "Found $versionsCount versions"

  if [ "$latestVersionsCount" -gt 0 ]; then
    echo "$latestVersionsCount latest versions:"
    echo "$latestVersions"
  fi

  if [ -z "$action" ]; then
    exit
  fi

  latestVersion="$(echo "$versions" | head -n 1)"

  if [ "$action" = "next" ]; then
    nextVersion="$(getNextVersion "$latestVersion" "$step")"
  elif [ "$action" = "pre" ]; then
    nextVersion="$(createPrerelease "$latestVersion" "$step" "$prefix")"
  fi

  echo "Next version:"
  echo "$latestVersion -> $nextVersion"

  echo "To tag the current commit with this version:"
  echo "git tag $nextVersion"

  remotes="$(git remote)"

  for remote in $remotes; do
    echo "git push $remote $nextVersion"
  done
elif [ "$action" = "latest" ]; then
  stable="$2"

  if [ -n "$stable" ] && [ "$stable" != "stable" ]; then
    usage
    exit 1
  fi

  checkGit

  versions="$(getVersionTags)"

  if [ -z "$stable" ]; then
    latestVersion="$(echo "$versions" | head -n 1)"

    echo "Latest version:"
  else
    latestVersion="$(echo "$versions" | grep -E '^v[0-9]+\.[0-9]+.[0-9]+$' | head -n 1)"

    echo "Latest stable version:"
  fi

  echo "$latestVersion"
else
  usage
  exit 1
fi

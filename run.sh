#!/bin/sh

set -e

imageName="semver-scripts-run"

usage() {
  echo "$(basename "$0") image <image> [command...] - create a container from image 'image' and run 'command...' on it."
  echo "$(basename "$0") setup                      - install missing packages (git). Supports apk, apt-get and dnf."
  echo "$(basename "$0") clean                      - clean images."
}

hasCommands() {
  for command in "$@"; do
    if ! [ "$(command -v "$command")" ]; then
      return 1
    fi
  done

  return 0
}

action="$1"

if [ "$action" = "image" ]; then
  image="$2"

  if [ -z "$image" ]; then
    usage
    exit 1
  fi

  shift 2

  dockerfile="$(printf "FROM %s\nWORKDIR /src\nCOPY --from=src . .\nRUN ./run.sh setup" "$image")"
  imageTag="$imageName:$(printf "%s" "$image" | sed 's/[:\/]/_/g')"

  docker pull --quiet "$image" >/dev/null
  printf "%s" "$dockerfile" | docker build --quiet --tag "$imageTag" --build-context src=. - >/dev/null
  docker run --rm -it -v "$(pwd)/.":/src "$imageTag" "$@"
elif [ "$action" = "setup" ]; then
  if ! hasCommands git; then
    if hasCommands apk; then
      apk add --no-cache --quiet git
    elif hasCommands apt-get; then
      apt-get -qq update
      apt-get -qq install -y git >/dev/null
    elif hasCommands dnf; then
      dnf install -y git
    else
      echo "ERROR: Setup requires apk, apt or dnf package manager"
      exit 1
    fi
  fi

  git config --global --add safe.directory /src
elif [ "$action" = "clean" ]; then
  images="$(docker image ls --filter reference="$imageName" --format '{{.ID}}')"
  docker image rm $images
else
  usage
  exit 1
fi
#!/bin/sh

set -e

containerRegistryUrl="$1"
containerRegistryUsername="$2"
containerRegistryPassword="$3"
imageName="$4"
imageTag="$5"

if [ -z "$containerRegistryUrl" ] || [ -z "$containerRegistryUsername" ] || [ -z "$containerRegistryPassword" ] || [ -z "$imageName" ] || [ -z "$imageTag" ]; then
  echo "Usage:"
  echo "$(basename "$0") <containerRegistryUrl> <containerRegistryUsername> <containerRegistryPassword> <imageName> <imageTag>"
  exit 1
fi

tags="$imageTag"

if (echo "$imageTag" | grep -Eq '^v[0-9]+\.[0-9]+\.[0-9]+$'); then
  echo "$imageTag is a semantic version. Checking for current version tags..."

  gitVersionTags="$(git tag --list 'v*')"

  tagMajor="$(echo "$imageTag" | sed -E 's/^v([0-9]+)\.([0-9]+)\.([0-9]+)$/\1/')"
  tagMinor="$(echo "$imageTag" | sed -E 's/^v([0-9]+)\.([0-9]+)\.([0-9]+)$/\2/')"

  currentLatestStableVersion="$(echo "$gitVersionTags" | grep -E '^v[0-9]+\.[0-9]+\.[0-9]+$' | sort -V -r | head -n 1)"
  currentLatestRelatedMinorVersion="$(echo "$gitVersionTags" | grep -E "^v${tagMajor}\.[0-9]+\.[0-9]+$" | sort -V -r | head -n 1)"
  currentLatestRelatedPatchVersion="$(echo "$gitVersionTags" | grep -E "^v${tagMajor}\.${tagMinor}\.[0-9]+$" | sort -V -r | head -n 1)"

  echo "Latest stable version: $currentLatestStableVersion."
  echo "Latest related minor v${tagMajor}.*.* version: $currentLatestRelatedMinorVersion."
  echo "Latest related patch v${tagMajor}.${tagMinor}.* version: $currentLatestRelatedPatchVersion."

  latestRelatedStableVersion="$(echo "$currentLatestStableVersion $imageTag" | sed -E 's/\s+/\n/' | sort -V -r | head -n 1)"

  if [ "$imageTag" = "$latestRelatedStableVersion" ]; then
    echo "LATEST STABLE: Tag $imageTag is newer or the same as the current latest stable version $currentLatestStableVersion. Will tag the image as :latest as well."
    tags="$tags latest"
  fi

  latestRelatedMinorVersion="$(echo "$currentLatestRelatedMinorVersion $imageTag" | sed -E 's/\s+/\n/' | sort -V -r | head -n 1)"

  if [ "$imageTag" = "$latestRelatedMinorVersion" ]; then
    echo "LATEST MINOR: Tag $imageTag is newer or the same as the current latest minor v${tagMajor}.*.* version $currentLatestRelatedMinorVersion. Will tag the image as :v${tagMajor} as well."
    tags="$tags v${tagMajor}"
  fi

  latestRelatedPatchVersion="$(echo "$currentLatestRelatedPatchVersion $imageTag" | sed -E 's/\s+/\n/' | sort -V -r | head -n 1)"

  if [ "$imageTag" = "$latestRelatedPatchVersion" ]; then
    echo "LATEST MINOR: Tag $imageTag is newer or the same as the current latest patch v${tagMajor}.${tagMinor}.* version $currentLatestRelatedPatchVersion. Will tag the image as :v${tagMajor}.${tagMinor} as well."
    tags="$tags v${tagMajor}.${tagMinor}"
  fi
fi

echo "Preparing to publish image named $imageName with tags:"

for tag in $tags; do
  echo "- $tag"
done

tagsArg=""
buildArgs="--build-arg APP_VERSION=$imageTag"

for tag in $tags; do
  tagsArg="$tagsArg --tag $imageName:$tag"
done

echo "Logging into the container registry at $containerRegistryUrl..."
docker login --username "$containerRegistryUsername" --password "$containerRegistryPassword" "$containerRegistryUrl"

echo "Building image with:"
echo "docker build $buildArgs $tagsArg ."
docker build $buildArgs $tagsArg .

echo "Pushing images to the registry..."
docker push --all-tags "$imageName"